# Heritability meaning and computation

As any data-driven institution, the CGIAR requires KPIs to condense breeding data into meaningful metrics that allow breeding programs to assess the overall performance and specific processes. The heritability have been proposed as a key metric to review the quality of the evaluation activities. In this GitLab page we provide scripts and data to recreate examples mentioned in the "how to" manual "Heritability: meaning and computation.pdf" available in the toolbox of EiB (https://excellenceinbreeding.org/toolbox/tools/eib-breeding-scheme-optimization-manuals). Please refer to this file to understand the scripts and data stored in this project.

## literature folder

Contains the papers reviewed to develop the "Heritability: meaning and computation.pdf" file.

## scripts folder

Contains scripts to recreate the simulation and analysis for different types of heritability estimates using three different mixed model software (asreml, sommer, lme4):

+ Cullis
+ Pipho
+ Standard